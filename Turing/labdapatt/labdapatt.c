#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main(void)
{
	WINDOW *ablak;
	ablak = initscr();

	int x=0;
	int y=0;

	int dx=1;
	int dy=1;

	int mx;
	int my;

	for (;;)
	{
		getmaxyx (ablak,my,mx);

		mvprintw(y,x,"O");

		refresh();
		usleep(100000);

		clear();

		x=x+dx;
		y=x+dy;

		if (x>=mx-1){
			dx=dx*-1;
		}
		if (x<=0){
			dx=dx*-1;
		}
		if (y<=0){
			dy=dy*-1;
		}
		if (y>my-1){
			dy=dy*-1;
		}
	}
	return 0;
}